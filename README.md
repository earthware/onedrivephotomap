﻿One Drive Map Sample Project
=========================

Demo: http://onedrivemap.earthware.co.uk/

This is a simple JavaScript project demonstrating how to use the Windows Live JavaScript controls to read OneDrive camera roll photos and plot these on a Bing Maps.

All the code is client side in JavaScript and there is some attempt to write the code in a OO style using the revealing prototype pattern.

Feel free to use this project however you wish but please do not abuse either of the client keys and make sure youn register your own at http://www.bingmapsportal.com and https://account.live.com/developers/applications/
