﻿/// <reference path="///LiveSDKHTML/js/wl.js" />
var ODPhotoMap = function () {

};

ODPhotoMap.prototype = function () {
    // manage ids at https://account.live.com/developers/applications/
    var appClientId = "000000004C11C967",
        redirectUrl = $(location).attr('protocol') + "//" + $(location).attr('hostname') + "/callback.html",
        map = null,
        ib = null,
        // basic init function
        load = function () {
            setupMap();
            setupoLiveApi();
            requestPhotosAccess();
            
            
        },
        // setup the login for live services
        setupoLiveApi = function()
        {
            WL.Event.subscribe("auth.login", onLogin);
            WL.init({
                client_id: appClientId,
                redirect_uri: redirectUrl,
                scope: "wl.signin",
                response_type: "token"
            });
            WL.ui({
                name: "signin",
                element: "signin"
            });
        },
        // request permissions for photos
        requestPhotosAccess = function() {
            WL.login({ "scope": "wl.skydrive" }).then(loadPhotos,
                function (response) {
                    log("Could not connect, status = " + response.status);
                }
            );
        },
        // handler for live logged in event
        onLogin = function() {
            
        },
        // request a specific folder or file path
        getFolderOrFiles = function(path, callback)
        {
            WL.api({ path: path, method: "GET" }).then(
                function (response) {
                    callback(response);
                    //log(JSON.stringify(response).replace(/,/g, ",\n"));
                },
                function (response) {
                    log("Could not access albums, status = " +
                        JSON.stringify(response.error).replace(/,/g, ",\n"));
                }
            );
        },
        // load the photos from camera foll and call pushpin plot
        loadPhotos = function () {
            $('#loadingmsg').modal('show');
            getFolderOrFiles("me/skydrive/camera_roll/files", plotPhotoPin);
        },
        // loop the returned data and plot pins
        plotPhotoPin = function (data) {
            var locs = [];
            $.each(data.data, function (index, file) {
                if (file.location) {
                    var pushpin = new Microsoft.Maps.Pushpin(new Microsoft.Maps.Location(file.location.latitude, file.location.longitude), { width: 54, height: 54, icon: file.images[2].source, anchor: { x: 50, y: 50 } });
                    pushpin.file = file;
                    map.entities.push(pushpin);
                    // add click handler
                    Microsoft.Maps.Events.addHandler(pushpin, 'click', pushpinClicked);
                    locs.push(file.location);
                }
            });

            $('#loadingmsg').modal('hide');

            if(locs.length) map.setView({ bounds: Microsoft.Maps.LocationRect.fromLocations(locs) });
        },
        // setup the bing map
        setupMap = function () {
            Microsoft.Maps.loadModule('Microsoft.Maps.Themes.BingTheme', { callback: function() {
                Microsoft.Maps.loadModule('Microsoft.Maps.Overlays.Style', {
                    callback: function () {
                        map = new Microsoft.Maps.Map($('#map')[0], { credentials: 'ArjTlHHOjL2ZjOtEZn7AN2OyrJAut3BP3g1SVw8g0hRLw3I5a2e_eiAy83bc_5EJ', disableBirdseye: true, customizeOverlaysmap: true, theme: new Microsoft.Maps.Themes.BingTheme() });
                    }
                });
            } });
        },
        pushpinClicked = function (ev) {
            var zoom = map.getZoom() > 16 ? map.getZoom() : 16;
            map.setView({ center: ev.target.getLocation(), zoom: zoom });
            showInfoBox(ev.target);
        },
        showInfoBox = function (pin) {
            // create ib if not already
            if (!ib) {
                //ib = new Microsoft.Maps.Infobox({
                //    location: map.getCenter(), visble: false
                //});
                ib = new Microsoft.Maps.Infobox(new Microsoft.Maps.Location(0, 0), { visible: false, offset: { x: 0, y: 54}, height:300 });
                map.entities.push(ib);
            }
            
            // move and open
            ib.setOptions({ location: pin.getLocation(), visible: true, title: pin.file.when_taken, description: "<img src='" + pin.file.images[1].source + "' width='176' height='99' /><br /><a class='btn btn-default' href='" + pin.file.link + "' target='_blank'>View On OneDrive</a> " });
            ib.setLocation(pin.getLocation());
        }
        // log function for debug
        log = function(message) {
            var child = document.createTextNode(message);
            var parent = document.getElementById('JsOutputDiv') || document.body;
            parent.appendChild(child);
            parent.appendChild(document.createElement("br"));
        };

    return {
        load: load
    };
}();

var od;
$(function() {
    od = new ODPhotoMap();
    od.load();
});
